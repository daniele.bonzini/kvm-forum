{% assign edition = site.editions[-1] %}

KVM Forum is an annual event that presents a rare opportunity for
developers and users to discuss the state of Linux virtualization
technology and plan for the challenges ahead. Sessions include updates
on the state of the KVM virtualization stack, planning for the future,
and many opportunities for attendees to collaborate.  Birds of a feather
(BoF) sessions facilitate discussion of strategic decisions.

## KVM Forum {{ edition.title }} schedule now available!

The next KVM Forum will be held in **[{{ edition.place }}](/location)**
on **{{ edition.dates | replace: "-", "&ndash;" }}**.
**[Consult the schedule here](/{{ edition.title }}/schedule/)**.

## Attending KVM Forum

{% if edition.register %}
The cost of admission to KVM Forum {{ edition.title }} is $75. You can
register [here]({{edition.register}}).  Admission is free for accepted speakers.

**If you need a visa invitation letter** in order
to attend, please reach out to the organizers at
[kvm-forum-2024-pc@redhat.com](mailto:kvm-forum-2024-pc@redhat.com).

Booking at the special conference rate at Hotel International is now
closed.  If you have booked a room and wish to cancel, please reach out
to the organizers at
[kvm-forum-2024-pc@redhat.com](mailto:kvm-forum-2024-pc@redhat.com).
{% else %}
Registration to KVM Forum {{ edition.title }} is not open yet.
{% endif %}

We are committed to fostering an open and welcoming environment at our
conference. Participants are expected to abide by our [code
of conduct](coc/) and [media policy](media-policy/).
