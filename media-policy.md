---
title: Media policy
heading: "Media policy"
layout: home
permalink: /media-policy/
---
{% include relative_root.html %}

tldr; {{ site.title }} organizers strive to create a positive
environment that adheres to our Code of Conduct and respects privacy
of individuals. That said, this is a public event and {{ site.title }}
event organizers do not wish to discourage folks from taking and sharing
photos and video during the event. Also, all sessions to be recorded and
posted on our YouTube channel for archival purposes, unless otherwise
explicitly requested.

## Speakers and session recording

When you agree to speak at any {{ site.title }} event, you consent
for your talk to be recorded in audio and possibly video format. All
sessions are recorded by official {{ site.title }} or venue A/V
staff. Additionally, you agree to permitting the recording and sharing
of sessions by attendees of the event at large. If you do not wish for
the audience to audio/video record, you are responsible for politely
asking them not to record you before you start presenting.

We reserve the right to publicly distribute and publish these recordings
under the Creative Commons Attribution 3.0 license.

Your session material, including content and recordings may be used in
promotional activities and other related endeavors. This material may
also appear on our web site, social media channels or other digital
media controlled by the {{ site.title }} organizers.

As conference organizers, we cannot be all places at once and it is
important for us to be able to review all material presented at the
conference. For this reason, you may not opt out of having your talk
recorded. However, if you wish, you may request that the session video
not be published to the public or taken down (hidden) if otherwise
already published. The organizers will, to the best of their ability,
respect and honors all such requests.

By default, all sessions will be streamed live and archived for future
playback on our {{ site.title }} YouTube channel.

## Staff and attendee photography

When you register to participate at {{ site.title }}, you consent to be
photographed by our official conference photographer, and to have those
photographs published under a Creative Commons license and possibly used
in various promotional material.

Additionally, we encourage attendees to take and share their own
photographs. Thus, you also consent to allow attendees to take and share
their own photographs, as long as the rules below are followed.

* Any photography/recording that is legally allowed in public spaces is
allowed at {{ site.title }}.
* Photography/recording should be treated like other potentially harassing
interpersonal interaction. That is, when one person in the interaction
says “stop” or “leave me alone” (etc), the interaction must
end. In this case, attendees should not attempt to photograph that
individual again. Additionally, please respect the no-photography
indicators worn by attendees.
* Photography/recording should not be done in such a way as to hide from
the subject that it’s happening.
* The subject may inspect the photo/recording at any time and, if
requested, the photo/footage/etc must be deleted immediately, upon
request.
* Those who are taking photographs and/or recording must also follow
our general Code of Conduct.

We will do our best to not make or share photos, videos or audio
recordings public which could cause harm to the individuals shown/heard on
them. If you do find such pictures/videos/audio recordings on one of our
sites or accounts, please write to our info@devconf.cz, info@devconf.in
or info@devconf.us to ask for review.

## How to opt-out

If you do not wish images of yourself to be published, you may opt out
by contacting the organizers on site or by email, and reasonable efforts
will be done to accommodate your request.

## How to report violations or issues

Please report an violations or issues with recording or photography directly on site, or by email at
[kvm-forum-{{edition.title}}-pc@redhat.com](mailto:kvm-forum-{{edition.title}}-pc@redhat.com).

_Thank you to Linux Fest NorthWest, Open Source Bridge and DrupalCon, from whom this policy was heavily influenced._
