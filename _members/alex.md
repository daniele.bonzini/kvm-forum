---
name: Alexander Graf
role: Principal Software Engineer - Amazon Web Services (AWS)
---
Alexander currently works at AWS and is responsible for the Nitro
Hypervisor. Previously, he worked on QEMU, KVM, openSUSE / SLES on
ARM and U-Boot. Whenever something really useful comes to his mind, he
implements it. Among others he did Mac OS X virtualization using KVM,
nested SVM, KVM on PowerPC, a lot of work in QEMU for openSUSE on ARM
and the UEFI compatibility layer in U-Boot.

