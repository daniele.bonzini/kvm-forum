**UPDATE**: KVM Forum 2023 will be held at [IMPACT HUB Brno](https://goo.gl/maps/CioY4ky9GMsurES29).

The venue is within walking distance from the train station (less than
1 km) and can be reached easily with public transportation, for example:

* from downtown Brno with trams 9 and 10 (stop Vlhká)
* from Hotel Avanti with bus 67 (stop Vlhká)

The easiest way to use trams, trolleybuses or buses in Brno is
to tap a credit card against the yellow reader every time you
board a vehicle and when you leave. The system will charge you
90 CZK at maximum per day. For information about other options,
including multi-day tickets, please consult the [DevConf.CZ web
site](https://www.devconf.info/cz/aroundbrno/).

## Getting to Brno

Brno has a small international airport with flights from London (Stansted) and other European cities.

Other nearby airports include Vienna, Bratislava and Prague. Travelling
to Brno is easiest from Vienna Schwechat Airport, from where there are
direct buses operated by [RegioJet](https://regiojet.com/?fromLocationId=10204055&toLocationId=10202002)
(formerly known as Student Agency).

More detailed information are available on the [DevConf.CZ website](https://www.devconf.info/cz/tobrno/).

If you need a visa invitation letter, please reach out to the organizers at
[kvm-forum-2023-pc@redhat.com](mailto:kvm-forum-2023-pc@redhat.com).

## Accomodation

Special room prices are available at several hotels in Brno for attendees
of both KVM Forum and DevConf.CZ. See [the DevConf.CZ hotels page](https://www.devconf.info/cz/hotels/)
for more information; note that KVM Forum does not provide accomodation
for speakers.
