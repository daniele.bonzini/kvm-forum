---
dates: "October 21-22, 2013"
place: "Edinburgh, Scotland"
layout: archive
sponsors:
  - type: "Host Sponsor"
    who: ["Red Hat (old logo)"]
  - type: "Platinum Sponsor"
    who: ["IBM"]
  - type: "Gold Sponsor"
    who: ["HPE", "OVA"]
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2013).

Video recordings are available on [YouTube](https://www.youtube.com/playlist?list=PLW3ep1uCIRfwAVDPuXffWz5qjcrXcpXVw).

# Pictures

* Group photo ([1](https://www.flickr.com/photos/linuxfoundation/10610967316/sizes/o/in/set-72157636827139203/), [2](https://www.flickr.com/photos/linuxfoundation/10610965526/sizes/o/in/set-72157636827139203/))
