## Announcing KVM Forum {{ site.editions[-1].title }}!

KVM Forum is an annual event that presents a rare opportunity for
developers and users to discuss the state of Linux virtualization
technology and plan for the challenges ahead. Sessions include updates
on the state of the KVM virtualization stack, planning for the future,
and many opportunities for attendees to collaborate.  Birds of a feather
(BoF) sessions facilitate discussion of strategic decisions.

The next KVM Forum will be held in {{ site.editions[-1].dates }}.
