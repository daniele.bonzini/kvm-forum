Website content is licensed under the
Creative Commons Attribution-ShareAlike 4.0 International public license.

Design licensed under Creative Commons Attribution 3.0 Unported.  The theme
is a derivative of [Linear](https://jekyll-demos.github.io/Linear-Jekyll-Theme/)
by TEMPLATED.

Presentations are redistributed with permission of the authors.
